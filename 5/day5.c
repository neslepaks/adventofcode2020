#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define max(a,b)	((a) > (b) ? (a) : (b))

int cmp(const void *a, const void *b) {
	return *(const int *)a > *(const int *)b;
}

int main(void) {
	FILE *fp = fopen("input", "r");
	char c;
	int i = 0;
	int row0 = 0, row1 = 127;
	int seat0 = 0, seat1 = 7;
	int seat, row;
	int id, maxid = 0;
	int all_ids[1024] = { 0 };
	int all_id_counter = 0, last_id = 0;

	while ((c = getc(fp)) != EOF) {
		if (c == '\n') {
			id = row * 8 + seat;

			maxid = max(maxid, id);
			printf("row %d, column %d, seat ID %d\n",
					row, seat, id);
			all_ids[all_id_counter++] = id;
			i = 0;
			row0 = 0; row1 = 127;
			seat0 = 0; seat1 = 7;
			continue;
		}
		if (i < 7) {
			if (c == 'F')	row1 -= ((row1+1)-row0)/2;
			else		row0 += ((row1+1)-row0)/2;
			if (i == 6)	row = c == 'F' ? row0 : row1;
		}
		else {
			if (c == 'L')	seat1 -= ((seat1+1)-seat0)/2;
			else		seat0 += ((seat1+1)-seat0)/2;
			if (i == 9)	seat = c == 'L' ? seat0 : seat1;
		}

		i++;
	}
	printf("highest seat ID: %d\n", maxid);

	qsort(&all_ids, maxid, sizeof (int), cmp);
	for (all_id_counter = 0; all_id_counter < maxid; all_id_counter++) {
		if (all_ids[last_id]+1 != all_ids[all_id_counter]) {
			printf("MISSING: %d\n", all_ids[last_id]+1);
		}
		last_id = all_id_counter;
	}

	return 0;
}

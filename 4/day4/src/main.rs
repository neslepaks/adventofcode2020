use std::fs::File;
use std::io::{self, BufRead};

fn validate_field(key: &str, word: &str) -> bool {
    let wlen = word.len();
    match key {
        "byr:" => match &word[4..].parse::<u32>() {
            Ok(num) => num >= &1920 && num <= &2002,
            Err(_)  => false
        },
        "iyr:" => match &word[4..].parse::<u32>() {
            Ok(num) => num >= &2010 && num <= &2020,
            Err(_)  => false
        },
        "eyr:" => match &word[4..].parse::<u32>() {
            Ok(num) => num >= &2020 && num <= &2030,
            Err(_)  => false
        },
        "hgt:" => match &word[wlen-2..] {
            "in" => match &word[4..wlen-2].parse::<u32>() {
                Ok(num) => num >= &59 && num <= &76,
                Err(_)  => false
            },
            "cm" => match &word[4..wlen-2].parse::<u32>() {
                Ok(num) => num >= &150 && num <= &193,
                Err(_)  => false
            },
            _ => false
        },
        "hcl:" => match &word[4..5] {
            "#" => wlen == 11 && word[5..].chars().all(|c|
                                                       (c >= '0' && c <= '9' ||
                                                        c >= 'a' && c <= 'f')),
            _   => false
        },
        "ecl:" => match &word[4..] {
            "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => true,
            _ => false
        },
        "pid:" => wlen == 13 && word[4..].chars().all(|c| c >= '0' && c <= '9'),
        _ => false
    }
}

fn process_passport(words: &Vec<String>) -> u32 {
    let mut count = 0;
    for word in words {
        for key in ["byr:", "iyr:", "eyr:", "hgt:",
                    "hcl:", "ecl:", "pid:"].iter() {
            if word.contains(key) && validate_field(&key, &word) {
                count += 1;
            }
        }
    }
    count
}

fn main() -> Result<(), std::io::Error> {
    let file = File::open("../input")?;
    let lines = io::BufReader::new(file).lines();
    let mut words: Vec<String> = vec![];
    let mut valid = 0;
    let mut linecount = 0;
    for linemaybe in lines {
        let line = linemaybe?;
        linecount += 1;
        if line.is_empty() {
            if process_passport(&words) == 7 {
                valid += 1;
            } else {
                println!("invalid: {:?}", &words);
            }
            words.truncate(0);
            continue;
        }
        for word in line.split_whitespace() {
            words.push(String::from(word));
        }
    }
    if process_passport(&words) == 7 {
        valid += 1;
    }
    println!("lines: {}, valid: {}", linecount, valid);
    Ok(())
}

import itertools

def is_sum_of(n, numbers):
    for pair in itertools.combinations(numbers, 2):
        if pair[0]+pair[1] == n:
            return True
    return False

def part1(filename, window_size):
    start, stop = 0, window_size
    numbers = [int(n) for n in open(filename).readlines()]

    assert len(numbers[start:stop]) == window_size
    for n in numbers[window_size:]:
        if not is_sum_of(n, numbers[start:stop]):
            return n
        start += 1
        stop += 1

def part2(filename, window_size):
    numbers = [int(n) for n in open(filename).readlines()]

    n = part1(filename, window_size)

    start = 0
    while start < n:
        i = start+1
        while i < len(numbers):
            if sum(numbers[start:i]) == n:
                return min(numbers[start:i]) + max(numbers[start:i])
            i += 1
        start += 1

if __name__ == '__main__':
    print(part1("exinput", 5))
    print(part1("input", 25))
    print(part2("exinput", 5))
    print(part2("input", 25))

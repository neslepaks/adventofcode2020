def execute(oplist):
    acc = 0
    cnt = 0
    lines_visited = []

    while cnt < len(oplist):
        if cnt in lines_visited:
            raise ValueError(f"Instruction revisit, acc={acc}")
        lines_visited.append(cnt)

        op, arg = oplist[cnt]
        if op == "nop":
            cnt += 1
        elif op == "acc":
            acc += arg
            cnt += 1
        elif op == "jmp":
            cnt += arg
    return acc

def parse(filename):
    lines = [l.split() for l in open(filename).readlines()]
    return [(op, int(arg)) for op, arg in lines]

def part1(filename):
    try:
        execute(parse(filename))
    except ValueError as e:
        return e

def part2(filename):
    oplist = parse(filename)
    for i, (op, arg) in enumerate(oplist):
        if op in ("nop", "jmp"):
            newop = "jmp" if op == "nop" else "nop"
            oplist[i] = (newop, arg)
            try:
                return execute(oplist)
            except ValueError:
                oplist[i] = (op, arg)

if __name__ == '__main__':
    for filename in ("exinput", "input"):
        print("part1 {}: {}".format(filename, part1(filename)))
    for filename in ("exinput", "input"):
        print("part2 {}: {}".format(filename, part2(filename)))

import re


class Day7:
    def __init__(self, filename):
        self.lines = open(filename).readlines()
        self.hits = set()
        self.count = 0

    def rgrep(self, bag):
        for i, line in enumerate(self.lines):
            if line.startswith(bag):
                continue
            if bag in line:
                self.hits.add(i)
                containing = ' '.join(line.split(' ')[0:2])
                self.rgrep(containing)

    def traverse(self, bag):
        rule = [line for line in self.lines if line.startswith(bag)][0]
        if rule.endswith('no other bags.'):
            return
        for count, bag in self.parserule(rule):
            self.count += int(count)
            for i in range(int(count)):
                self.traverse(bag)

    def parserule(self, rule):
        return re.findall('(\\d+) (\\w+ \\w+)', rule)


if __name__ == '__main__':
    for filename in ("exinput", "input"):
        part1 = Day7(filename)
        part1.rgrep("shiny gold")
        print(f"part1 {filename}: {len(part1.hits)}")

    for filename in ("exinput2", "input"):
        part2 = Day7(filename)
        part2.traverse("shiny gold")
        print(f"part2 {filename}: {part2.count}")

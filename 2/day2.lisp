(defun day2 ()
  (verify-passwords (parse-input)))

(defun day2-2 ()
  (verify-passwords-2 (parse-input)))

(defun parse-input ()
  (with-open-file (in "adventofcode2020/2/input")
    (loop :for l := (read-line in nil nil) :while l :collect (parse-line l))))

(defun parse-line (line)
  (destructuring-bind (from to char password) (cl-ppcre:split "[-: ]+" line)
    (list (parse-integer from) (parse-integer to) (char char 0) password)))

(defun verify-passwords (spec-list)
  (let ((valid 0) (invalid 0))
    (dolist (spec spec-list (list valid invalid))
      (destructuring-bind (from to char password) spec
	(if (<= from (count char password) to)
	    (incf valid)
	    (incf invalid))))))

(defun verify-passwords-2 (spec-list)
  (let ((valid 0) (invalid 0))
    (dolist (spec spec-list (list valid invalid))
      (destructuring-bind (from to char password) spec
	(if (= 1 (count t (list (char= char (char password (1- from)))
				(char= char (char password (1- to))))))
	    (incf valid)
	    (incf invalid))))))

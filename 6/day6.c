#include <stdio.h>

#define TABLEN	26

int read_chunk(FILE *fp, int part) {
	int tab[TABLEN] = {0};
	char c, last_c = 0;
	int i, count = 0;
	int records = 0;

	while ((c = getc(fp)) != EOF) {
		if (c == '\n') {
			if (last_c == '\n') {
				break;
			} else {
				records++;
				last_c = c;
				continue;
			}
		}

		tab[c-'a']++;
		/* printf("tab[%c] = %d\n", c, tab[c-'a']); */
			
		last_c = c;
	}
	/* printf("%d records\n", records); */

	for (i = 0; i < TABLEN; i++) {
		if (part == 1 && tab[i] > 0)
			count++;
		else if (part == 2 && tab[i] == records)
			count++;
	}

	return count;
}

void process_file(char *filename, int part) {
	FILE *fp = fopen(filename, "r");
	int count, total = 0;

	while (!feof(fp)) {
		count = read_chunk(fp, part);
		total += count;
	}

	printf("%s part%d total: %d\n", filename, part, total);
}

int main(void) {
	process_file("exinput", 1);
	process_file("input", 1);

	process_file("exinput", 2);
	process_file("input", 2);

	return 0;
}

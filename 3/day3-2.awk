BEGIN {
	n=RIGHT+1
	for (i=1; i<=DOWN; i++) getline
}
{
	if (substr($0, n, 1) == "#")
		trees++;
	n+=RIGHT
	if (n > length($0))
		n-=length($0)
	for (i=1; i<DOWN; i++) getline
}
END {
	print trees
}

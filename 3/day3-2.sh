#!/bin/sh

{
	awk -f day3-2.awk -v DOWN=1 -v RIGHT=1 input
	awk -f day3-2.awk -v DOWN=1 -v RIGHT=3 input
	awk -f day3-2.awk -v DOWN=1 -v RIGHT=5 input
	awk -f day3-2.awk -v DOWN=1 -v RIGHT=7 input
	awk -f day3-2.awk -v DOWN=2 -v RIGHT=1 input
} | awk 'BEGIN {n=1.0} {n*=$0} END {printf "%.0f\n", n}'

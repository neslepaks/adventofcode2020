BEGIN {
	n=4
	getline
}
{
	if (substr($0, n, 1) == "#")
		trees++;
	n+=3
	if (n > length($0))
		n-=length($0)
}
END {
	print trees
}

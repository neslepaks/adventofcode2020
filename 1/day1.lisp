(defun day1 ()
  (with-open-file (in "input")
    (let ((lines (loop :for l := (read-line in nil nil) :while l :collect l)))
      (find-2020 (mapcar #'parse-integer lines)))))

(defun day1-2 ()
  (with-open-file (in "input")
    (let ((lines (loop :for l := (read-line in nil nil) :while l :collect l)))
      (find-2020-3 (mapcar #'parse-integer lines)))))

(defun find-sum-pair (numbers &key sum)
  (when numbers
    (if (find (- sum (first numbers)) (rest numbers))
	(list (first numbers) (- sum (first numbers)))
	(find-sum-pair (rest numbers) :sum sum))))

(defun find-sum-triplet (numbers &key sum)
  (when numbers
    (let ((pair (find-sum-pair (rest numbers) :sum (- sum (first numbers)))))
      (if pair
	  (list* (first numbers) pair)
	  (find-sum-triplet (rest numbers) :sum sum)))))

(defun find-2020 (numbers &key (target 2020))
  (labels ((try-one (n1 list)
	     (if (null list)
		 nil
		 (if (= target (+ n1 (first list)))
		     (* n1 (first list))
		     (try-one n1 (rest list))))))
    (when numbers
      (or (try-one (first numbers) (rest numbers))
	  (find-2020 (rest numbers) :target target)))))

(defun find-2020-3 (numbers)
  (when numbers
    (let ((res (find-2020 (rest numbers) :target (- 2020 (first numbers)))))
      (if res
	  (* (first numbers) res)
	  (find-2020-3 (rest numbers))))))
